package me.cian.parser.model.aggregator.thread;

import me.cian.parser.model.aggregator.thread.collector.DataCollector;
import me.cian.parser.model.aggregator.thread.worker.DataWorker;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by 1 on 02.02.2017.
 */
@ContextConfiguration({
        "classpath:spring/applicationContext-test.xml",
})
@RunWith(SpringJUnit4ClassRunner.class)
public abstract class WorkerTest {
    @Autowired
    private DataWorker dataWorker;

    @Autowired
    private DataCollector collector;

}
