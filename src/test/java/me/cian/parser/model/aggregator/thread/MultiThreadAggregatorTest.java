package me.cian.parser.model.aggregator.thread;

import me.cian.parser.model.aggregator.AdvertAggregator;
import me.cian.parser.repository.AdvertRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

/**
 * Created by 1 on 02.02.2017.
 */
@ContextConfiguration({
        "classpath:spring/applicationContext-test.xml",
})
@RunWith(SpringJUnit4ClassRunner.class)
public class MultiThreadAggregatorTest {

    @Autowired
    private AdvertAggregator aggregator;

    @Autowired
    private AdvertRepository repository;

    @Test
    public void testStart() throws Exception {
//        aggregator.start();
    }
}