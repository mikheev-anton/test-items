package me.cian.parser;

import me.cian.parser.model.aggregator.AdvertAggregator;
import org.springframework.context.support.GenericXmlApplicationContext;

import java.util.Arrays;

/**
 * Created by 1 on 01.02.2017.
 */
public class Aggregator {
    public static void main(String[] args) {
        GenericXmlApplicationContext context = new GenericXmlApplicationContext("spring/applicationContext.xml");

        AdvertAggregator bean = context.getBean(AdvertAggregator.class);

//        Агрегатор запускается, с тем количеством страниц, которые мы хотим посмотреть.
//        В папке entity есть enum, отвечающий за те типы жилья, которые нам нужны
        bean.start(50);
    }
}
