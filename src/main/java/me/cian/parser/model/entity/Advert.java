package me.cian.parser.model.entity;

import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

/**
 * Created by 1 on 27.01.2017.
 */
@Document(collection = Advert.COLLECTION_NAME)
public class Advert implements Serializable {

    public static final String COLLECTION_NAME = "adverts";

    private String url;// - URL объявления

//    не понял что это
    private String status;// - статус (скачано, распаршено, неправильное, нет доступа и т.п. в зависимости от алгоритма скачивания)

    private int advertNum;// - номер объявления на сайте

    private String balcony;// - наличие и тип балконов

    private String bathroom;// - наличие и тип санузла

    private String lift;// - наличие и тип лифта

    private String flatRepairs;// - качество ремонта (отделки)

    private Integer floor;// - этаж

    private int numberOfStoreysInBuilding;// - число этажей в здании

    private int numberOfRooms;// - число комнат

    private double latitude; // - широта

    private double longitude; // - долгота

    private String total; // - общая площадь

    private String living; // - жилая площадь

    private String currency; // - валюта

    private String price; // - цена

    private String region; // - регион продажи

    private String rawAddress; // - адрес

    private String realtyType; // - тип недвижимости "Квартира", "Квартира в новостройке", "Дом"

    private String text; // - текст объявления

    private LocalDateTime lastUpdate; // - время апдейта (скачивания)

    private LocalDateTime published; // - время публикации на сайте

    private String phone; // - телефон продавца

    private List<String> photoUrls; // - список URL'ов с фотографиями объекта


    public Advert() {
    }


    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getAdvertNum() {
        return advertNum;
    }

    public void setAdvertNum(int advertNum) {
        this.advertNum = advertNum;
    }

    public String getBalcony() {
        return balcony;
    }

    public void setBalcony(String balcony) {
        this.balcony = balcony;
    }

    public String getBathroom() {
        return bathroom;
    }

    public void setBathroom(String bathroom) {
        this.bathroom = bathroom;
    }

    public String getLift() {
        return lift;
    }

    public void setLift(String lift) {
        this.lift = lift;
    }

    public String getFlatRepairs() {
        return flatRepairs;
    }

    public void setFlatRepairs(String flatRepairs) {
        this.flatRepairs = flatRepairs;
    }

    public Integer getFloor() {
        return floor;
    }

    public void setFloor(Integer floor) {
        this.floor = floor;
    }

    public int getNumberOfStoreysInBuilding() {
        return numberOfStoreysInBuilding;
    }

    public void setNumberOfStoreysInBuilding(int numberOfStoreysInBuilding) {
        this.numberOfStoreysInBuilding = numberOfStoreysInBuilding;
    }

    public int getNumberOfRooms() {
        return numberOfRooms;
    }

    public void setNumberOfRooms(int numberOfRooms) {
        this.numberOfRooms = numberOfRooms;
    }

    public void setCenter(Double[] center) {
        this.latitude = center[0];
        this.longitude = center[1];
    }


    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getLiving() {
        return living;
    }

    public void setLiving(String living) {
        this.living = living;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getRawAddress() {
        return rawAddress;
    }

    public void setRawAddress(String rawAddress) {
        this.rawAddress = rawAddress;
    }

    public String getRealtyType() {
        return realtyType;
    }

    public void setRealtyType(String realtyType) {
        this.realtyType = realtyType;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public LocalDateTime getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(LocalDateTime lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public LocalDateTime getPublished() {
        return published;
    }

    public void setPublished(LocalDateTime published) {
        this.published = published;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public List<String> getPhotoUrls() {
        return photoUrls;
    }

    public void setPhotoUrls(List<String> photoUrls) {
        this.photoUrls = photoUrls;
    }

    @Override
    public String toString() {
        return "Advert{" +
                "numberOfRooms=" + numberOfRooms +
                ", url='" + url + '\'' +
                ", status='" + status + '\'' +
                ", advertNum='" + advertNum + '\'' +
                ", balcony='" + balcony + '\'' +
                ", bathroom='" + bathroom + '\'' +
                ", lift='" + lift + '\'' +
                ", flatRepairs='" + flatRepairs + '\'' +
                ", floor=" + floor +
                ", numberOfStoreysInBuilding=" + numberOfStoreysInBuilding +
                ", center=" + latitude + " | " + longitude +
                ", total='" + total + '\'' +
                ", living='" + living + '\'' +
                ", currency='" + currency + '\'' +
                ", price='" + price + '\'' +
                ", region='" + region + '\'' +
                ", rawAddress='" + rawAddress + '\'' +
                ", realtyType='" + realtyType + '\'' +
                ", text='" + text.substring(0,10) + '\'' +
                ", lastUpdate=" + lastUpdate +
                ", published=" + published +
                ", phone='" + phone + '\'' +
                ", photoUrls=" + photoUrls +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Advert advert = (Advert) o;

        if (advertNum != advert.advertNum) return false;
        if (Double.compare(advert.latitude, latitude) != 0) return false;
        if (Double.compare(advert.longitude, longitude) != 0) return false;
        return !(url != null ? !url.equals(advert.url) : advert.url != null);

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = url != null ? url.hashCode() : 0;
        result = 31 * result + advertNum;
        temp = Double.doubleToLongBits(latitude);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(longitude);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}
