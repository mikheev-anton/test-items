package me.cian.parser.model.entity;

/**
 * Created by 1 on 28.01.2017.
 */
public enum EntityType {
//    у новых квартир тот же url
    FLAT(
            "https://www.cian.ru/sale/flat/%d/",
            136_500_000
    ),
    SUBURBAN(
            "https://www.cian.ru/sale/suburban/%d/",
            10_000_000
    );

    private final String urlPatternAdvertPage;
    private final int startIdCount;

    EntityType(String urlAdvert, int startIdCount) {
        this.urlPatternAdvertPage = urlAdvert;
        this.startIdCount = startIdCount;
    }


    public String getUrlPatternAdvertPage() {
        return urlPatternAdvertPage;
    }

    public int getStartPageId() {
        return startIdCount;
    }
}
