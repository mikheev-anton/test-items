package me.cian.parser.model.aggregator.thread.worker;

import me.cian.parser.model.aggregator.thread.Address;
import me.cian.parser.model.aggregator.thread.collector.DataCollector;
import me.cian.parser.model.aggregator.thread.task.ParseDataTask;
import me.cian.parser.model.aggregator.thread.task.SaveDataTask;
import me.cian.parser.model.aggregator.thread.task.Task;
import me.cian.parser.model.entity.Advert;
import me.cian.parser.model.to.AdvertData;
import me.cian.parser.util.*;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by 1 on 28.01.2017.
 */
public class AdvertDataParser implements DataWorker {

    private DataCollector collector;
    private AdvertData data;
    private Advert advert;
    private Address address;

    private JSONParser jsonParser;

    public AdvertDataParser(Address address, DataCollector collector) {
        this.address = address;
        this.collector = collector;
        this.jsonParser = new JSONParser();
    }

    @Override
    public void run() {
        Queue<Task> massage = collector.getTask(getAddress());
        doWork(massage);
    }

    @Override
    public void doWork(Queue<Task> tasks) {
        try {
            while (collector.isLoading() || !tasks.isEmpty()) {
                ParseDataTask poll = (ParseDataTask) tasks.poll();
                if (poll != null) {
                    this.data = poll.getData();
                    this.advert = new Advert();
                    try {
                        parsingJsonData();
                        parsingHTMLData();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    sendMadeTask();
                }
                wait(tasks);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void wait(Queue<Task> tasks) {
        if (tasks.isEmpty() && collector.isLoading()) {
            synchronized (tasks) {
                try {
                    tasks.wait();
                } catch (InterruptedException ignore) {
                }
            }
        }
    }

    private void parsingJsonData() throws ParseException {
        AdvertData data = this.data;
        String cleanJson = getCleanJson(data.getJsonData());
        JSONObject parse = (JSONObject) this.jsonParser.parse(cleanJson);
        JSONObject mainObject = (JSONObject) parse.get(String.valueOf(data.getId()));

        JSONObject priceObject = (JSONObject) mainObject.get("price");
        JSONArray center = (JSONArray) mainObject.get("center");
        JSONArray photos = (JSONArray) mainObject.get("photos");

        setRealtyType(mainObject);
        setCurrency(priceObject);
        setPrice(priceObject);
        setPhotoUrls(photos);
        setCenter(center);
        setFloor(mainObject);
        setNumberOfStoreysInBuilding(mainObject);
        setPhone(mainObject);
        setNumberOfRooms(mainObject);
        setTotal(mainObject);
    }

    private void parsingHTMLData() throws ParseException {
        Document parse = Jsoup.parse(data.getHtmlData());

        Elements descrAddr = parse.getElementsByClass("object_descr_addr");
        Elements children = descrAddr.first().children();
        Elements text = parse.getElementsByClass("object_descr_text");
        Elements tableLines = parse.select("tbody > tr");
        String tagWithDate = parse.select("[href=\"#\"]").first().attr("ng-click");
        String url = parse.select("[property=og:url]").attr("content");
        String fullAddress = getFullAddress(children);

        Map<String, String> table = convertLinesToMap(tableLines);

        setUrl(url);
        setPublished(tagWithDate);
        setText(text);
        setRawAddress(fullAddress);
        setRegion(fullAddress);
        setAdvertNum(data.getId());
        setLastUpdate(data.getDownloadDate());
        setLiving(table.get("area"));
        setBathroom(table.get("toilet"));
        setBalcony(table.get("balcony"));
        setLift(table.get("lift"));
        setFlatRepairs(table.get("repairs"));
    }

    private Map<String, String> convertLinesToMap(Elements tableLines) {
        HashMap<String, String> lines = new HashMap<>();

        for (Element e : tableLines) {
            String key = e.select("th").toString();
            String value = e.select("td").text();
            if (key.contains("Жилая площадь")) {
                lines.put("living", value);
            } else if (key.contains("Санузел") || key.contains("санузлов")) {
                lines.put("bathroom", value);
            } else if (key.contains("Балкон")) {
                lines.put("balcony", value);
            } else if (key.contains("Лифт")) {
                lines.put("lift", value);
            } else if (key.contains("Ремонт")) {
                lines.put("repairs", value);
            }
        }
        return lines;
    }

    private String getCleanJson(String jsonData) {
        String data = jsonData.replaceAll("\\s", "");
        Pattern pattern = Pattern.compile(VerifierUtil.JSON_REGEX);
        Matcher matcher = pattern.matcher(data);
        if (matcher.find()) {
            return matcher.group();
        }
        return "{}";
    }

    private void setUrl(String url) {
        String result = VerifierUtil.isValid(url) ? url : "";
        this.advert.setUrl(result);
    }

    private void setPublished(String tagWithDate) throws ParseException {
        LocalDateTime publicationDate = null;
        if (VerifierUtil.isValid(tagWithDate)) {
            JSONObject parse1 = (JSONObject) jsonParser.parse(getCleanJson(tagWithDate));
            String date = String.valueOf(parse1.get("publication_date"));
            String validDate = VerifierUtil.validDate(date);
            int epochDate = Integer.valueOf(validDate);
            publicationDate = LocalDateTime.ofEpochSecond(epochDate, 0, ZoneOffset.of("+03:00"));
        }

        this.advert.setPublished(publicationDate);
    }

    private void setLastUpdate(LocalDateTime downloadDate) {
        this.advert.setLastUpdate(downloadDate);
    }

    private void setRealtyType(JSONObject jsonObject) {
        String realtyType = "";
        if (VerifierUtil.isValid(jsonObject)) {
            String offerType = String.valueOf(jsonObject.get("offer_type"));
            boolean isPrimary = Boolean.parseBoolean(String.valueOf(jsonObject.get("is_primary")));
            realtyType = isPrimary ? "new_" + offerType : offerType;
        }
        this.advert.setRealtyType(realtyType);
    }

    private void setCurrency(JSONObject jsonObject) {
        String val = "";
        if (VerifierUtil.isValid(jsonObject)) {
            Map.Entry firstEntry = (Map.Entry) jsonObject.entrySet().iterator().next();
            val = String.valueOf(firstEntry.getKey());
        }
        this.advert.setCurrency(val);
    }

    private void setPrice(JSONObject jsonObject) {
        String priceTotal = "";
        if (VerifierUtil.isValid(jsonObject)) {
            Map.Entry firstEntry = (Map.Entry) jsonObject.entrySet().iterator().next();
            priceTotal = String.valueOf(firstEntry.getValue());
        }
        this.advert.setPrice(priceTotal);
    }

    private void setPhotoUrls(JSONArray jsonArray) {
        List<String> result = new ArrayList<>();

        if (VerifierUtil.isValid(jsonArray)) {
            List<JSONObject> p = new ArrayList<>(jsonArray);
            String bigImgSize = "-1.";
            String middleImgSize = "-2.";
            for (JSONObject s : p) {
                String img = String.valueOf(s.get("img"));
                String bigImg = img.replace(middleImgSize, bigImgSize);
                result.add(bigImg);
            }
        }
        this.advert.setPhotoUrls(result);
    }

    private void setCenter(JSONArray jsonArray) {
        Double[] center = new Double[2];
        if (VerifierUtil.isValid(jsonArray)) {
            center[0] = (Double) jsonArray.get(0);
            center[1] = (Double) jsonArray.get(1);
        }
        this.advert.setCenter(center);
    }

    private void setFloor(JSONObject jsonObject) {
        String checkFloor = String.valueOf(jsonObject.get("floor"));
        int floor = 0;
        if (VerifierUtil.isValid(checkFloor)) {
            floor = Integer.valueOf(checkFloor);
        }
        this.advert.setFloor(floor);
    }

    private void setNumberOfStoreysInBuilding(JSONObject jsonObject) {
        String floors_count = String.valueOf(jsonObject.get("floors_count"));
        int floorsCount = 0;
        if (VerifierUtil.isValid(floors_count)) {
            floorsCount = Integer.valueOf(floors_count);
        }
        this.advert.setNumberOfStoreysInBuilding(floorsCount);
    }

    private void setPhone(JSONObject jsonObject) {
        String phone = "";
        if (VerifierUtil.isValid(jsonObject)) {
            String check = String.valueOf(jsonObject.get("phone"));
            phone = VerifierUtil.isValid(check) ? check : "";
        }
        this.advert.setPhone(phone);
    }

    private void setNumberOfRooms(JSONObject jsonObject) {
        int roomsCount = 0;
        if (VerifierUtil.isValid(jsonObject)) {
            String rooms_count = String.valueOf(jsonObject.get("rooms_count"));
            roomsCount = VerifierUtil.isValid(rooms_count) ? Integer.valueOf(rooms_count) : 0;
        }
        this.advert.setNumberOfRooms(roomsCount);
    }

    private void setTotal(JSONObject jsonObject) {
        String totalArea = String.valueOf(jsonObject.get("total_area"));
        this.advert.setTotal(totalArea);
    }

    private String getFullAddress(Elements children) {
        StringBuilder b = new StringBuilder();
        int size = children.size();
        for (int i = 0; i < size; i++) {
            b.append(children.get(i).text());
            if (i == 0) {
                b.append("|");
            } else if (i != size - 1) {
                b.append(",");
            }
        }
        return b.toString();
    }

    private void setRawAddress(String fullAddress) {
        String[] split = fullAddress.split("\\|");
        this.advert.setRawAddress(split[1]);
    }

    private void setRegion(String fullAddress) {
        String[] split = fullAddress.split("\\|");
        this.advert.setRegion(split[0]);
    }

    private void setLiving(String value) {
        this.advert.setLiving(value);
    }

    private void setLift(String value) {
        this.advert.setLift(value);
    }

    private void setBalcony(String value) {
        this.advert.setBalcony(value);
    }

    private void setBathroom(String value) {
        this.advert.setBathroom(value);
    }

    private void setFlatRepairs(String value) {
        this.advert.setFlatRepairs(value);
    }

    private void setText(Elements text) {
        advert.setText(text.text());
    }

    private void setAdvertNum(int advertNum) {
        this.advert.setAdvertNum(advertNum);
    }

    private void sendMadeTask() {
        SaveDataTask task = new SaveDataTask(new Address(3), this.advert);
        collector.addTask(task);
    }

    @Override
    public Address getAddress() {
        return this.address;
    }
}
