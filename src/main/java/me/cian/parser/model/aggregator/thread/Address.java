package me.cian.parser.model.aggregator.thread;

/**
 * Created by 1 on 29.01.2017.
 */
public class Address {
    final int id;

    public Address(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Address address = (Address) o;

        return id == address.id;

    }

    @Override
    public int hashCode() {
        return id;
    }
}
