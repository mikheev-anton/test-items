package me.cian.parser.model.aggregator.thread.worker;

import me.cian.parser.model.aggregator.thread.Address;
import me.cian.parser.model.aggregator.thread.collector.DataCollector;
import me.cian.parser.model.aggregator.thread.task.LoadDataTask;
import me.cian.parser.model.aggregator.thread.task.ParseDataTask;
import me.cian.parser.model.aggregator.thread.task.Task;
import me.cian.parser.model.entity.EntityType;
import me.cian.parser.model.to.AdvertData;
import me.cian.parser.util.VerifierUtil;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.time.LocalDateTime;
import java.util.Queue;


/**
 * Created by 1 on 28.01.2017.
 */
public class AdvertDataLoader implements DataWorker {

    private final Address address;
    private DataCollector collector;

    public AdvertDataLoader(Address address, DataCollector dataCollector) {
        this.collector = dataCollector;
        this.address = address;
    }

    @Override
    public void run() {
        Queue<Task> massage = collector.getTask(getAddress());
        doWork(massage);
        this.collector.stop();
    }

    public void doWork(Queue<Task> tasks) {
       try {
           LoadDataTask loadTask = (LoadDataTask) tasks.peek();

           while (!tasks.isEmpty()) {
               Integer stopCount = loadTask.getStopCount();
               EntityType loadType = loadTask.getLoadType();
               try {
                   int pageNumber = loadTask.getPageId();
                   if (pageNumber > stopCount){
                       tasks.remove(loadTask);
                       loadTask = (LoadDataTask) tasks.peek();
                       continue;
                   }
                   String page = downloadPage(loadType.getUrlPatternAdvertPage(), pageNumber);
                   if (!page.isEmpty()){
                       createDataForWork(page, pageNumber);
                   }
               } catch (IOException ignore) {}
           }
       }catch (Exception e){
           e.printStackTrace();
       }
    }

    public String downloadPage(String urlPattern, int page) throws IOException {
        URL url = new URL(String.format(urlPattern, page));
        URLConnection spoof = url.openConnection();
        spoof.setRequestProperty( "User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36" );

        String strLine = "";

        StringBuilder finalHTML = new StringBuilder();

        try (BufferedReader in = new BufferedReader(new InputStreamReader(spoof.getInputStream()));){
            while ((strLine = in.readLine()) != null){
                finalHTML.append(strLine);
            }
        }catch (FileNotFoundException ignore){

        }

        return finalHTML.toString();
    }

    private void createDataForWork(String page, int pageNumber){
        Address destination = new Address(2);
        Document document = Jsoup.parse(page);
        String htmlData = getCleanHtml(document);
        String jsonData = getCleanJsonData(document);
        if (jsonData.isEmpty() || htmlData.isEmpty()){
            return;
        }
        AdvertData advertData = new AdvertData(pageNumber, htmlData, jsonData, LocalDateTime.now());
        ParseDataTask parseTask = new ParseDataTask(destination, advertData);
        collector.addTask(parseTask);
    }

    private String getCleanJsonData(Document document) {
        Elements scripts = document.head().select("script");
        for (Element e : scripts){
            String scriptTag = e.toString();
            if (scriptTag.contains("window._offers")){
                return scripts.html();
            }
        }
        return "";
    }

    private String getCleanHtml(Document page) {
        String tagWithUrl = VerifierUtil.verifyUrlHtml(page.select("[property=og:url]").toString());

        if (tagWithUrl.isEmpty())
            return "";

        String tagWithTable = page.getElementsByClass("object_descr_dt_row").html();
        String tagWithDate = page.select(".object_descr_td_l").toString();
        return tagWithTable + tagWithDate + tagWithUrl;
    }

    @Override
    public Address getAddress() {
        return address;
    }
}
