package me.cian.parser.model.aggregator.thread;

import me.cian.parser.model.aggregator.AdvertAggregator;
import me.cian.parser.model.aggregator.thread.task.LoadDataTask;
import me.cian.parser.model.aggregator.thread.worker.AdvertDataSaver;
import me.cian.parser.model.entity.EntityType;
import me.cian.parser.model.aggregator.thread.worker.AdvertDataLoader;
import me.cian.parser.model.aggregator.thread.worker.AdvertDataParser;
import me.cian.parser.model.aggregator.thread.collector.DataCollector;
import me.cian.parser.model.aggregator.thread.worker.DataWorker;
import me.cian.parser.repository.AdvertRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by 1 on 01.02.2017.
 */
@Component
public class MultiThreadAggregator implements AdvertAggregator {

    private ExecutorService executor;

    private int loaders;
    private int savers;
    private int parsers;

    @Autowired
    private AdvertRepository repository;

    @Autowired
    private DataCollector collector;

    public MultiThreadAggregator(@Value("30") int threadCount) {
        this.executor = Executors.newFixedThreadPool(threadCount);
        this.loaders = (int)(threadCount * 0.5);
        this.savers = (threadCount - loaders)/2;
        this.parsers = threadCount - savers - loaders;
    }

    @Override
    public void start(int countPageCheck){

        createStartTask(countPageCheck);
        startLoadThreads();
        startParsingThreads();
        startSavingThreads();

        executor.shutdown();
    }

    private void createStartTask(int countPageLoad){
        Address loaderAddress = new Address(1);
        for (EntityType type : EntityType.values()){
            LoadDataTask task = new LoadDataTask(loaderAddress, type, type.getStartPageId() + countPageLoad);
            collector.addTask(task);
        }
    }

    private void startSavingThreads() {
        Address address = new Address(3);
        for (int i = 0; i < savers; i++) {
            DataWorker r = new AdvertDataSaver(address, collector, repository);
            executor.submit(r);
        }
    }

    private void startParsingThreads() {
        Address address = new Address(2);
        for (int i = 0; i < parsers; i++) {
            DataWorker r = new AdvertDataParser(address, collector);
            executor.submit(r);
        }
    }

    private void startLoadThreads() {
        Address address = new Address(1);
        this.collector.setCountOfLoaders(this.loaders);
        for (int i = 0; i < loaders; i++) {
            DataWorker customAdvertPageLoader = new AdvertDataLoader(address, collector);
            executor.submit(customAdvertPageLoader);
        }
    }
}
