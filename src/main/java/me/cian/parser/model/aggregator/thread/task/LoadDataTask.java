package me.cian.parser.model.aggregator.thread.task;

import me.cian.parser.model.aggregator.thread.Address;
import me.cian.parser.model.entity.EntityType;

import java.util.concurrent.atomic.AtomicInteger;


/**
 * Created by 1 on 01.02.2017.
 */
public class LoadDataTask extends Task {

    private final AtomicInteger pageCounter;
    private final Integer stopCount;
    private final EntityType loadType;

    public LoadDataTask(Address to, EntityType loadType, Integer stopCount) {
        super(to);
        this.stopCount = stopCount;
        this.loadType = loadType;
        this.pageCounter = new AtomicInteger(loadType.getStartPageId());
    }

    public Integer getStopCount() {
        return stopCount;
    }

    public EntityType getLoadType() {
        return loadType;
    }

    public int getPageId() {
        return pageCounter.incrementAndGet();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LoadDataTask that = (LoadDataTask) o;

        if (stopCount != null ? !stopCount.equals(that.stopCount) : that.stopCount != null) return false;
        return loadType == that.loadType;

    }

    @Override
    public int hashCode() {
        int result = stopCount != null ? stopCount.hashCode() : 0;
        result = 31 * result + (loadType != null ? loadType.hashCode() : 0);
        return result;
    }
}
