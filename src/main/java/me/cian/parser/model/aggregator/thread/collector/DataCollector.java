package me.cian.parser.model.aggregator.thread.collector;

import me.cian.parser.model.aggregator.thread.Address;
import me.cian.parser.model.aggregator.thread.task.Task;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.concurrent.*;

/**
 * Created by 1 on 29.01.2017.
 */
@Component
public class DataCollector {

    private Map<Address, ConcurrentLinkedQueue<Task>> task;
    private volatile boolean isLoading;

    private int countOfLoaders;

    public DataCollector() {
        this.task = new ConcurrentHashMap<>();
        this.task.put(new Address(1), new ConcurrentLinkedQueue<>());
        this.task.put(new Address(2), new ConcurrentLinkedQueue<>());
        this.task.put(new Address(3), new ConcurrentLinkedQueue<>());
        this.isLoading = true;
    }

    public void addTask(Task task){
        Queue<Task> tasks = this.task.get(task.getTo());
        tasks.add(task);
        synchronized (tasks){
            tasks.notifyAll();
        }
    }

    public Queue<Task> getTask(Address address){
        return task.get(address);
    }

    public boolean isLoading() {
        return isLoading;
    }

    public void stop(){
        isLoading = false;
        this.countOfLoaders--;
        if (this.countOfLoaders <= 0){
            for (Queue<Task> t : task.values()){
                synchronized (t){
                    t.notifyAll();
                }
            }
        }
    }

    public void setCountOfLoaders(int countOfLoaders) {
        if (this.countOfLoaders <= 0){
            this.countOfLoaders = countOfLoaders;
        }
    }
}
