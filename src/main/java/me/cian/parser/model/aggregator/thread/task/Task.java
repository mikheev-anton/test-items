package me.cian.parser.model.aggregator.thread.task;

import me.cian.parser.model.aggregator.thread.Address;

/**
 * Created by 1 on 29.01.2017.
 */
public abstract class Task {
    protected Address to;

    public Task(Address to) {
        this.to = to;
    }

    public Address getTo() {
        return to;
    }
}
