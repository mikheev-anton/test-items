package me.cian.parser.model.aggregator.thread.worker;

import me.cian.parser.model.aggregator.thread.Address;
import me.cian.parser.model.aggregator.thread.task.Task;

import java.util.Queue;

/**
 * Created by 1 on 29.01.2017.
 */
public interface DataWorker extends Runnable {
    Address getAddress();
    void doWork(Queue<Task> tasks);
}
