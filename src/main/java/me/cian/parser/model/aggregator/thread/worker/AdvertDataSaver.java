package me.cian.parser.model.aggregator.thread.worker;

import me.cian.parser.model.aggregator.thread.collector.DataCollector;
import me.cian.parser.model.aggregator.thread.task.SaveDataTask;
import me.cian.parser.model.aggregator.thread.task.Task;
import me.cian.parser.model.entity.Advert;
import me.cian.parser.model.aggregator.thread.Address;
import me.cian.parser.repository.AdvertRepository;

import java.util.Queue;

/**
 * Created by 1 on 31.01.2017.
 */
public class AdvertDataSaver implements DataWorker {

    private AdvertRepository repository;
    private final DataCollector collector;
    private final Address address;

    public AdvertDataSaver(Address address,DataCollector collector, AdvertRepository repository) {
        this.address = address;
        this.collector = collector;
        this.repository = repository;
    }

    @Override
    public void run() {
        Queue<Task> massages = this.collector.getTask(getAddress());
        doWork(massages);
    }

    @Override
    public void doWork(Queue<Task> tasks) {
        try {
            while (collector.isLoading() || !tasks.isEmpty()){
                SaveDataTask poll = (SaveDataTask) tasks.poll();
                if (poll != null){
                    Advert advert = poll.getAdvert();
//                    System.out.println(advert);
                    this.repository.save(advert);
                }
                wait(tasks);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void wait(Queue<Task> tasks) {
        if (tasks.isEmpty() && collector.isLoading()){
            synchronized (tasks){
                try {
                    tasks.wait();
                } catch (InterruptedException ignore) {}
            }
        }
    }

    @Override
    public Address getAddress() {
        return this.address;

    }

}
