package me.cian.parser.model.aggregator.thread.task;

import me.cian.parser.model.aggregator.thread.Address;
import me.cian.parser.model.entity.Advert;

/**
 * Created by 1 on 30.01.2017.
 */
public class SaveDataTask extends Task {

    private final Advert advert;

    public SaveDataTask(Address to, Advert advert) {
        super(to);
        this.advert = advert;
    }

    public Advert getAdvert() {
        return this.advert;
    }
}
