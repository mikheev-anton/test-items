package me.cian.parser.model.aggregator.thread.task;

import me.cian.parser.model.aggregator.thread.Address;
import me.cian.parser.model.to.AdvertData;


/**
 * Created by 1 on 29.01.2017.
 */
public class ParseDataTask extends Task {

    private final AdvertData data;

    public ParseDataTask(Address to, AdvertData data) {
        super(to);
        this.data = data;
    }

    public AdvertData getData() {
        return data;
    }
}
