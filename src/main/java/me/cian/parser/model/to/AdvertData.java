package me.cian.parser.model.to;

import java.time.LocalDateTime;

/**
 * Created by 1 on 28.01.2017.
 */
public class AdvertData {
    private final int advertId;
    private final LocalDateTime downloadDate;
    private final String htmlData;
    private final String jsonData;

    public AdvertData(int id, String htmlData, String jsonData, LocalDateTime downloadDate) {
        this.advertId = id;
        this.htmlData = htmlData;
        this.jsonData = jsonData;
        this.downloadDate = downloadDate;
    }

    public int getId() {
        return advertId;
    }

    public String getHtmlData() {
        return htmlData;
    }

    public String getJsonData() {
        return jsonData;
    }

    public LocalDateTime getDownloadDate() {
        return downloadDate;
    }
}
