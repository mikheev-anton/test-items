package me.cian.parser.repository.nosql;

import me.cian.parser.model.entity.Advert;
import me.cian.parser.repository.AdvertRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;

/**
 * Created by 1 on 31.01.2017.
 */
@Repository
public class MongoDBAdvertRepository implements AdvertRepository {

    @Autowired
    private MongoOperations mongoOperations;

    @Override
    public void save(Advert advert) {
        mongoOperations.save(advert);
    }

    @Override
    public boolean delete(int id) {
        return false;
    }

    @Override
    public Advert get(int id) {
        return mongoOperations.findOne(Query.query(Criteria.where("id").is(id)), Advert.class);
    }

    @Override
    public Collection<Advert> getAll() {
        return null;
    }
}
