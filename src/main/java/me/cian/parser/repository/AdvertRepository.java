package me.cian.parser.repository;

import me.cian.parser.model.entity.Advert;

import java.util.Collection;

/**
 * Created by 1 on 31.01.2017.
 */
public interface AdvertRepository {

    void save(Advert meal);

    boolean delete(int id);

    Advert get(int id);

    Collection<Advert> getAll();
}
