package me.cian.parser.util;

import me.cian.parser.model.entity.EntityType;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.jsoup.select.Elements;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by 1 on 01.02.2017.
 */
public class VerifierUtil {


    public static final String JSON_REGEX = "\\{(?:\\s*\".*?(?=([^\\\\](?:\\\\\\\\)*\"))\\1\\s*:\\s*" +
            "(?:\".*?(?=([^\\\\](?:\\\\\\\\)*\"))\\2|null|false|true|(?:-?\\d+|\\d*\\.\\d+)" +
            "(?:e[+-]?\\d+)?)\\s*,)*\\s*\".*?(?=([^\\\\](?:\\\\\\\\)*\"))\\3\\s*:\\s*(?:\".*?" +
            "(?=([^\\\\](?:\\\\\\\\)*\"))\\4|null|false|true|(?:-?\\d+|\\d*\\.\\d+)(?:e[+-]?\\d+)?)\\s*\\}+";

    public static boolean isValid(JSONArray array){
        return Objects.nonNull(array);
    }

    public static boolean isValid(String str){
        return Objects.nonNull(str)&&!str.isEmpty()&&!str.equals("null");
    }

    public static boolean isValid(JSONObject obj){
        return Objects.nonNull(obj);
    }

    public static boolean isValid(Elements elem){
        return Objects.nonNull(elem);
    }

    public static String validDate(String date){
        Pattern pattern = Pattern.compile("\\d+");
        Matcher matcher = pattern.matcher(date);
        return matcher.matches() ? date : "000000000000";
    }


    public static String verifyUrlHtml(String html){
        String result = "";
        for (EntityType s : EntityType.values()){
            if (html.contains(s.toString().toLowerCase())){
                result = html;
                break;
            }
        }
        return result;
    }
}
